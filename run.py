from app.app import config_app

if __name__ == "__main__":
    app = config_app("prod")
    app.run(debug=True, port=5000)
