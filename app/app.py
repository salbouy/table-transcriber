import json
import os
from pathlib import Path

import torch
import werkzeug
from IPython.display import Image
from PIL import Image
from flask import (
    Flask,
    request,
    render_template,
    redirect,
    flash,
    url_for,
    session,
    jsonify,
    make_response,
)
from werkzeug.exceptions import BadRequestKeyError
from werkzeug.utils import secure_filename

from app.TableExtractor.src_segm.transcriber import Transcriber
from app.TableExtractor.src_segm.utils.const import (
    ILLUSTRATION_LABEL,
    TEXT_LABEL,
    LINE_LABEL,
    COLUMN_LABEL,
    TABLE_LABEL,
    EMPTY_TABLE
)
from app.utils.const import (
    APP_NAME,
    CONFIG,
    TEMPLATE_DIR,
    UPLOAD_DIR,
    STATIC_DIR,
    OUTPUT_DIR,
)
from app.utils.utils import empty_dir, create_if_not, get_json_content, create_dir
from app.TableExtractor.src_segm.utils import check_extension

app = Flask(__name__, template_folder=TEMPLATE_DIR, static_folder=STATIC_DIR)


def log(variable):
    if isinstance(variable, (dict, list)):
        app.logger.info(json.dumps(variable, indent=4))
    else:
        app.logger.info(variable)


@app.context_processor
def global_jinja_variables():
    return {"app_title": APP_NAME}


@app.errorhandler(404)
def handle_bad_request(e):
    log(e)
    if "favicon" not in request.path:
        flash(f"Bad request: something wrong with the URL '{request.url}'", "error")
    # return render_template("pages/import.html", title="Bad request"), 404
    return (
        redirect(url_for("transcriber")),
        404,
        {"Refresh": f"1; url={url_for('transcriber')}"},
    )


@app.route("/psw", methods=["GET", "POST"])
def request_access():
    # Here, a method to check if the user has the correct password to access the app
    return render_template("pages/index.html", title="Logging")


@app.route("/", methods=["GET", "POST"])
def transcriber():
    # Here, a method to retrieve the image(s) uploaded by the user and store it in /temp
    # Then, detects the cells in the image(s), transcribe them
    # Then returns an object that will be transformed into an HTML table in order to be corrected by the user

    # # # IMAGE UPLOAD
    if request.method == "POST":
        f = request.files["file"]
        if f:
            if check_extension(f.filename):
                try:
                    idx = 0  # todo: find a way to give new id to new transcription
                    image_folder = create_dir(UPLOAD_DIR / f"{idx}")
                    # note: for now, segmentation do not work if several images are stored in the folder
                    empty_dir(image_folder)
                    filepath = image_folder / secure_filename(f.filename)
                    f.save(filepath)

                    html_translator = Transcriber(
                        transcript_nb=idx,
                        image_folder=image_folder,
                        labels_to_extract=[
                            ILLUSTRATION_LABEL,
                            TEXT_LABEL,
                            TABLE_LABEL,
                            LINE_LABEL,
                            COLUMN_LABEL,
                        ],
                    )
                    try:
                        output = html_translator.segment() or EMPTY_TABLE
                        print(output)
                        with open(
                            OUTPUT_DIR / f"segmented_{idx}.json", "w+"
                        ) as outfile:
                            json.dump(output, outfile)
                        return redirect(url_for("prepare", idx=idx))
                    except Exception:
                        flash("No table was detected within the image!", "error")
                except IOError as e:
                    app.logger.exception(e)
                    flash("Can't save and process file!", "error")
            else:
                flash("The file does not have the correct extension!", "error")
        else:
            flash("Error while importing the file!", "error")

    return render_template("pages/import.html", title="Import image")


@app.route("/prepare/<int:idx>", methods=["GET", "POST"])
def prepare(idx):
    # idx = request.args.get("id")
    if idx is None or type(idx) is not int:
        flash("No id provided: unable to proceed to preparation", "error")
        return redirect(url_for("transcriber"))

    if not os.path.isfile(OUTPUT_DIR / f"segmented_{idx}.json"):
        flash("No file associated to the id: unable to proceed to preparation", "error")
        return redirect(url_for("transcriber"))

    if request.method == "POST":
        try:
            rowCol = (
                request.json
                if request.json
                else get_json_content(OUTPUT_DIR / f"segmented_{idx}.json")
            )
        except IOError as e:
            app.logger.exception(e)
            flash("Can't retrieve selected rows and columns!", "error")
            return make_response(jsonify({"success": False}), 500)

        try:
            transcriptor = Transcriber(int(idx))
            with open(OUTPUT_DIR / f"transcribed_{idx}.json", "w+") as outfile:
                json.dump(transcriptor.transcribe(rowCol), outfile)
        except IOError as e:
            app.logger.exception(e)
            flash("Can't perform HTR on table!", "error")
            return make_response(jsonify({"success": False}), 500)

        return make_response(jsonify({"success": True}), 200)

    return render_template(
        "pages/prepare.html",
        output=get_json_content(OUTPUT_DIR / f"segmented_{idx}.json"),
        idx=idx,
        title="Prepare your table",
    )


@app.route("/annotate/<int:idx>", methods=["GET", "POST"])
def annotate(idx):
    # idx = request.args.get("id")
    if idx is None or type(idx) is not int:
        flash("No id provided: unable to proceed to transcription", "error")
        return redirect(url_for("transcriber"))

    filename = f"rotated_{idx}.jpg"
    try:
        img = Image.open(Path(OUTPUT_DIR) / filename)
        width, height = img.size
    except FileNotFoundError:
        flash(
            "No file associated to the id: unable to proceed to transcription", "error"
        )
        return redirect(url_for("transcriber"))

    try:
        output = get_json_content(OUTPUT_DIR / f"transcribed_{idx}.json")
    except json.decoder.JSONDecodeError:
        with open(OUTPUT_DIR / f"transcribed_{idx}.json") as f:
            content = f.readlines()
        log(content)
        flash(
            "Invalid data: unable to use it for transcription\n\n"
            "Transcription will be performed using data produced with segmentation",
            "error",
        )

        try:
            output = get_json_content(OUTPUT_DIR / f"segmented_{idx}.json")
        except json.decoder.JSONDecodeError:
            with open(OUTPUT_DIR / f"segmented_{idx}.json") as f:
                content = f.readlines()
            log(content)
            flash(
                "Invalid data: unable to proceed to transcription"
                f"Content: {content}",
                "error",
            )
            return redirect(url_for("transcriber"))

    # TODO: not rely on outfile but rather maybe on the update on a variable update
    # TODO: have a save button that save a state of transcription

    return render_template(
        "pages/annotate.html",
        image=filename,
        output=output,
        title="Annotate your table",
        dim=[width, height],
    )


@app.route("/test", methods=["GET", "POST"])
def test():
    return render_template(
        "pages/test.html",
        title="Test page",
    )


def config_app(config_name="test"):
    """ Create the application """
    app.config.from_object(CONFIG[config_name])
    return app


if __name__ == "__main__":
    app.run()
