import torch
from pathlib import Path
from app.utils.const import APP_DIR, ROOT_DIR

# Project and source files
# ROOT_DIR = (
#     os.path.dirname(os.path.abspath(__file__)).split("table-transcriber", 1)[0]
#     + "table-transcriber"
# )

ROOT_PATH = ROOT_DIR
PROJECT_PATH = APP_DIR / "TableExtractor"
MODELS_PATH = PROJECT_PATH / "models"
HTR_MODEL = MODELS_PATH / "HTR_model.pth"
SEGM_MODEL = MODELS_PATH / "SEGM_model1.pkl"
HTR_SRC = PROJECT_PATH / "src_HTR"
SEGM_SRC = PROJECT_PATH / "src_segm"

CUDA = "cuda" if torch.cuda.is_available() else "cpu"
