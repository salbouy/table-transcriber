from app.TableExtractor.utils.const import SEGM_SRC
from app.utils.const import APP_DIR

# Project and source files
PROJECT_PATH = APP_DIR / "TableExtractor"
CONFIGS_PATH = SEGM_SRC / "config"
RAW_DATA_PATH = SEGM_SRC / "raw_data"  # do not exist
RESULTS_PATH = SEGM_SRC / "results"  # do not exist
MODELS_PATH = PROJECT_PATH / "models"

# Synthetic
DATASETS_PATH = SEGM_SRC / "datasets"
SYNTHETIC_DOCUMENT_DATASET_PATH = DATASETS_PATH / "syndoc"  # do not exist
SYNTHETIC_LINE_DATASET_PATH = DATASETS_PATH / "synline"  # do not exist
SYNTHETIC_RESRC_PATH = PROJECT_PATH / "synthetic_resource"  # do not exist
