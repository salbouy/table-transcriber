from functools import wraps

from numpy.random import seed as np_seed
from numpy.random import get_state as np_get_state
from numpy.random import set_state as np_set_state
from random import seed as rand_seed
from random import getstate as rand_get_state
from random import setstate as rand_set_state
import torch
from torch import manual_seed as torch_seed
from torch import get_rng_state as torch_get_state
from torch import set_rng_state as torch_set_state

from app.utils.utils import create_dir, empty_dir
import shutil
from pathlib import Path
import os

import numpy as np
import rglob

from app.TableExtractor.src_segm.utils.const import (
    BACKGROUND_LABEL,
    COLUMN_LABEL,
    ILLUSTRATION_LABEL,
    LABEL_TO_COLOR_MAPPING,
    TABLE_WORD_LABEL,
    TEXT_LABEL,
    ADDITIONAL_MARGIN_RATIO,
    AREA_RATIO_THRESHOLD,
    VALID_EXTENSIONS,
)
from app.TableExtractor.src_segm.utils.image import resize
from app.utils.const import (
    OUTPUT_DIR,
    UPLOAD_DIR,
)


def check_extension(filename=""):
    """ Returns True if the file has a correct extension """
    print(filename)
    return "." in filename and filename.rsplit(".", 1)[1] in VALID_EXTENSIONS


class use_seed:
    def __init__(self, seed=None):
        if seed is not None:
            assert isinstance(seed, int) and seed >= 0
        self.seed = seed

    def __enter__(self):
        if self.seed is not None:
            self.rand_state = rand_get_state()
            self.np_state = np_get_state()
            self.torch_state = torch_get_state()
            self.torch_cudnn_deterministic = torch.backends.cudnn.deterministic
            rand_seed(self.seed)
            np_seed(self.seed)
            torch_seed(self.seed)
            torch.backends.cudnn.deterministic = True
        return self

    def __exit__(self, typ, val, _traceback):
        if self.seed is not None:
            rand_set_state(self.rand_state)
            np_set_state(self.np_state)
            torch_set_state(self.torch_state)
            torch.backends.cudnn.deterministic = self.torch_cudnn_deterministic

    def __call__(self, f):
        @wraps(f)
        def wrapper(*args, **kw):
            seed = self.seed if self.seed is not None else kw.pop("seed", None)
            with use_seed(seed):
                return f(*args, **kw)

        return wrapper


def retrieve_images():
    folder = UPLOAD_DIR
    types = ("*.jpg", "*.jpeg", "*.JPG", "*.tif")
    file_list = []
    for files in types:
        file_list.extend(rglob.rglob(folder, files))


def set_output_path():
    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)
    return empty_dir(OUTPUT_DIR)


def delete_close(coords, thresh):
    for i in range(len(coords) - 1):
        if coords[i + 1] - coords[i] < thresh:
            coords[i + 1] = coords[i]
    return list(dict.fromkeys(coords))


def img_dim(img):
    """
    @param img:
    @type img: Image
    @return:
    @rtype:
    """
    return img.size[0] * img.size[1]


def check_area_to_img_ratio(img, zone_dim, label):
    """
    @param img:
    @type img: Image
    @param zone_dim:
    @type zone_dim: int
    @param label:
    @type label:
    @return:
    @rtype:
    """
    return (zone_dim / img_dim(img)) >= AREA_RATIO_THRESHOLD[label]


def is_cell_in_table(x_tab, y_tab, x_txt, y_txt):
    return (
        np.array(x_tab).min() <= x_txt <= np.array(x_tab).max()
        and np.array(y_tab).min() <= y_txt <= np.array(y_tab).max()
    )


def is_in_range(col_idx, x_col, row_idx, y_row):
    return 0 < col_idx < len(x_col) and 0 < row_idx < len(y_row)
