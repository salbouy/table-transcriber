from functools import partial
from pprint import pprint

import torch

from app.utils.utils import check_path
from app.TableExtractor.utils.const import CUDA, SEGM_MODEL

from app.TableExtractor.src_segm.models.res_unet import ResUNet
from app.TableExtractor.src_segm.models.tools import safe_model_state_dict


def get_model(name=None):
    if name is None:
        name = "res_unet18"
    return {
        "res_unet18": partial(ResUNet, encoder_name="resnet18"),
        "res_unet34": partial(ResUNet, encoder_name="resnet34"),
        "res_unet50": partial(ResUNet, encoder_name="resnet50"),
        "res_unet101": partial(ResUNet, encoder_name="resnet101"),
        "res_unet152": partial(ResUNet, encoder_name="resnet152"),
    }[name]


def load_model_from_path(
    model_path=SEGM_MODEL, device=None, attributes_to_return=None, eval_mode=True
):
    if device is None:
        device = torch.device(CUDA)

    checkpoint = torch.load(check_path(model_path), map_location=device.type)
    checkpoint["model_kwargs"]["pretrained_encoder"] = False
    model = get_model(checkpoint["model_name"])(
        checkpoint["n_classes_1"],
        checkpoint["n_classes_2"],
        checkpoint["n_classes_3"],
        checkpoint["n_classes_4"],
        **checkpoint["model_kwargs"]
    ).to(device)

    model.load_state_dict(safe_model_state_dict(checkpoint["model_state"]))
    if eval_mode:
        model.eval()
    if attributes_to_return is not None:
        if isinstance(attributes_to_return, str):
            attributes_to_return = [attributes_to_return]

        return model, [
            sorted(checkpoint.get(attr))
            if "restricted_labels" in attr
            else checkpoint.get(attr)
            for attr in attributes_to_return
        ]
    else:
        return model
