import collections
import statistics
from bisect import bisect
from pathlib import Path
import sys

import cv2
import numpy as np
import torch
from PIL import Image, ImageDraw

from app.TableExtractor.src_HTR.line_predictor import transcribe
from app.TableExtractor.src_HTR.network import RCNN
from app.TableExtractor.src_HTR.params import BaseOptions
from app.TableExtractor.src_segm.models import load_model_from_path
from app.TableExtractor.src_segm.utils import (
    set_output_path,
    delete_close,
    is_cell_in_table,
    is_in_range,
    check_area_to_img_ratio,
    img_dim,
)

from app.TableExtractor.src_segm.utils.const import (
    BACKGROUND_LABEL,
    COLUMN_LABEL,
    ILLUSTRATION_LABEL,
    LABEL_TO_COLOR_MAPPING,
    TABLE_LABEL,
    TEXT_LABEL,
    LINE_LABEL,
    ADDITIONAL_MARGIN_RATIO,
    VALID_EXTENSIONS,
    DEFAULT_LABELS_TO_EXTRACT,
    ATTRIBUTES_TO_RETURN,
    ANNOTATION_COLOR,
    MARGIN_COLOR,
)
from app.TableExtractor.src_segm.utils.image import resize
from app.TableExtractor.utils.const import (
    SEGM_MODEL,
    HTR_SRC,
    HTR_MODEL,
    CUDA,
)
from app.utils.const import OUTPUT_DIR
from app.utils.utils import get_files_from_dir


class Transcriber:
    """
    Extract and transcribe tables from files in a given input_dir folder and save them in the provided output_dir.
    Supported input extensions are: jpg, png, tiff, pdf.
    Each input image is thought to contain 0 or 1 table (no more than 1).
    This program is thought to work on a four branches network, with the following labels:
    restricted_labels_1 = [1, 4, 6], restricted_labels_2 = [9], restricted_labels_3 = [14], restricted_labels_4 = [13]
    """

    def __init__(
        self,
        transcript_nb: int = 0,
        image_folder: Path = None,
        labels_to_extract: list = None,
        save_annotations: bool = True,
        straight_bbox: bool = False,
        add_margin: bool = True,
        draw_margin: bool = False,
        verbose: bool = False,
    ):
        self.device = torch.device(CUDA)
        self.output_path = set_output_path() if image_folder else OUTPUT_DIR
        self.images = []  # List[Image]
        self.model = None
        self.output = {}
        self.idx = transcript_nb

        self.img_size = None
        self.save_annotations = save_annotations
        self.straight_bbox = straight_bbox  # allows non-right angles in the detection of areas to extract
        self.add_margin = add_margin
        self.draw_margin = add_margin and draw_margin
        self.verbose = verbose
        self.normalize = None

        self.restr_labels = None
        self.restr_labels_1 = None  # text area labels
        self.restr_labels_2 = None  # table area labels
        self.restr_labels_3 = None  # column separators area labels
        self.restr_labels_4 = None  # row separators area labels
        self.labels_to_extract = None

        if image_folder:
            self.images = get_files_from_dir(
                image_folder,
                valid_extensions=VALID_EXTENSIONS,
                recursive=True,
                sort=False,
                to_img=True,
            )
            self.set_model(labels_to_extract)

    def set_model(self, labels_to_extract=DEFAULT_LABELS_TO_EXTRACT):
        """
        Verifies that the labels that must be predicted are e subset of the labels on which the model was trained
        @param labels_to_extract: Integers corresponding to the labels to detect within the images
        @type labels_to_extract: List[int]
        """
        self.model, (
            self.img_size,
            self.restr_labels_1,
            self.restr_labels_2,
            self.restr_labels_3,
            self.restr_labels_4,
            self.normalize,
        ) = load_model_from_path(SEGM_MODEL, self.device, ATTRIBUTES_TO_RETURN)

        self.model.eval()

        self.restr_labels = sorted(
            self.restr_labels_1
            + self.restr_labels_2
            + self.restr_labels_3
            + self.restr_labels_4
        )

        self.labels_to_extract = sorted(labels_to_extract)

        if not set(self.labels_to_extract).issubset(self.restr_labels):
            raise ValueError(
                "Incompatible `labels_to_extract` and `restricted_labels` arguments: "
                f"model was trained using {self.restr_labels} labels only"
                f"whereas {self.labels_to_extract} where required"
            )

    def run(self):
        """
        Perform areas detection, cell cropping and HTR transcription
        segment() > straighten_img() > extract_areas() > draw_separators() > cut_cells() > transcribe()
        """
        # marker: predict separators > cut cells + transcribe
        self.segment()
        self.transcribe()
        return self.output

    def segment(self):
        """
        For each image of the Transcriber class, find the table, straighten it according to its columns
        Then detect areas corresponding to the text, table, columns and rows and draw those areas on an annotated image
        Project column and row separators on the image and then cut cells according to text areas and the separators that were detected
        @return: Dictionary containing information on the detected cell images
        @rtype: Dict
        """
        for img in self.images:
            img = self.straighten_img(img)
            if img is None:
                print("No table was detected within the scan provided", file=sys.stdout)
                raise Exception("No table detected")

            (
                annotated_img,
                txt_areas,
                min_x_txt,
                max_x_txt,
                min_y_txt,
                max_y_txt,
                mean_x_txt,
                mean_y_txt,
                x_col,
                y_row,
                x_table,
                y_table,
            ) = self.extract_areas(img)
            # marker: change here y and x coordinates to change col/row separators
            self.draw_separators(annotated_img, y_row, x_col)

            self.cut_cells(
                txt_areas,
                mean_x_txt,
                mean_y_txt,
                min_x_txt,
                min_y_txt,
                max_x_txt,
                max_y_txt,
                y_row,
                x_col,
                x_table,
                y_table,
            )
        return self.output

    def save_annotated_img(self, annotated_img):
        """
        @param annotated_img:
        @type annotated_img: Image.Image
        """
        if self.save_annotations:
            annotated_img.save(f"{self.output_path}/annotated_{self.idx}.jpg")

    def draw_annotation(self, annotated_img, line, fill, width):
        if self.save_annotations:
            # lw = int(min([0.005 * img.size[0], 0.005 * img.size[1]]))
            # draw = ImageDraw.Draw(annotated_img)
            # draw.line(
            #     list(map(tuple, bbox.tolist())) + [tuple(bbox[0])],
            #     fill=ANNOTATION_COLOR,
            #     width=lw,
            # )

            draw = ImageDraw.Draw(annotated_img)
            draw.line(
                line,
                fill=fill,
                width=width,
            )
            self.save_annotated_img(annotated_img)

    def draw_separators(self, annotated_img, y_row, x_col):
        """
        @param annotated_img:
        @type annotated_img:
        @param y_row: vertical coordinates of all the rows
        @type y_row: List[int]
        @param x_col: horizontal coordinates of all the columns
        @type x_col: List[int]
        """
        # Draw the grid separators on the annotated image
        # it is not necessary, just in order to visualise where each separator was detected
        if self.save_annotations:
            draw = ImageDraw.Draw(annotated_img)
            for y in y_row:
                draw.line([(50, y), (100, y)], fill=255, width=10)
            for x in x_col:
                draw.line([(x, 50), (x, 100)], fill=255, width=10)

            self.save_annotated_img(annotated_img)

    def extract_areas(self, img):
        """
        Detect areas associated to restr_labels
        @type img: Image.Image
        @return:
        @rtype:
        """
        pred_1, pred_2, pred_3, pred_4 = self.predict(img)
        annotated_img = img.copy()

        txt_areas = None
        min_x_txt = []  # leftest points of each detected textual area
        max_x_txt = []  # rightest point of each detected textual area
        min_y_txt = []  # highest point of each detected textual area
        max_y_txt = []  # lowest point of each detected textual area
        mean_x_txt = []  # horizontal center of each detected textual area
        mean_y_txt = []  # vertical center of each detected textual area
        mean_x_col = []  # horizontal center of each detected column separator area
        mean_y_row = []  # vertical center of each detected row separator area
        x_table = []  # horizontal coordinates of the detected table area
        y_table = []  # vertical coordinates of the detected table area

        img_w, img_h = img.size

        # Extraction of all the different labels,
        # with their corresponding coordinates (in order to recognize the structure of the table)
        for label in self.labels_to_extract:
            if label == BACKGROUND_LABEL:
                continue

            if label in self.restr_labels_1:
                # Extraction of the text areas
                if label == TEXT_LABEL:
                    # Determination of the mean, and extreme coordinates for every textual area
                    txt_areas, x_txt, y_txt = self.draw_areas(
                        img, pred_1, label, annotated_img, "restr_labels_1"
                    )
                    # minimum x coordinates of all the textual areas
                    min_x_txt = [np.min(x) for x in x_txt]
                    # maximum x coordinates of all the textual areas
                    max_x_txt = [np.max(x) for x in x_txt]
                    # minimum y coordinates of all the textual areas
                    min_y_txt = [np.min(y) for y in y_txt]
                    # maximum y coordinates of all the textual areas
                    max_y_txt = [np.max(y) for y in y_txt]
                    # mean x coordinates of all the textual areas
                    mean_x_txt = [np.mean(x) for x in x_txt]
                    # mean y coordinates of all the textual areas
                    mean_y_txt = [np.mean(y) for y in y_txt]

                # else:
                # marker: is it really necessary to predict something in this case?
                # _, _, _ = self.draw_areas(
                #     img, pred_1, label, annotated_img
                # )

            elif label in self.restr_labels_2:
                # Extraction of the table areas
                _, x_table, y_table = self.draw_areas(
                    img, pred_2, label, annotated_img, "restr_labels_2"
                )

            elif label in self.restr_labels_3:
                # Extraction of the columns separators and determination of their coordinates
                _, x_col, _ = self.draw_areas(
                    img, pred_3, label, annotated_img, "restr_labels_3"
                )
                # MARKER: x_coord = list des x de toutes les columns
                mean_x_col = [np.mean(x) for x in x_col]
                # Deletion of too close separators (too close = image width / 150)
                mean_x_col = delete_close(
                    list(dict.fromkeys(np.sort(mean_x_col))),
                    img_w / 150,
                )

            elif label in self.restr_labels_4:
                # Extraction of the rows separators and determination of their coordinates
                _, _, y_row = self.draw_areas(
                    img, pred_4, label, annotated_img, "restr_labels_4"
                )
                # MARKER: y_coord = list des y de tous les rows
                mean_y_row = [np.mean(y) for y in y_row]
                # Deletion of too close separators (too close = image height / 100)
                mean_y_row = delete_close(
                    list(dict.fromkeys(np.sort(mean_y_row))),
                    img_h / 100,
                )
        return (
            annotated_img,
            txt_areas,
            min_x_txt,
            max_x_txt,
            min_y_txt,
            max_y_txt,
            mean_x_txt,
            mean_y_txt,
            mean_x_col,
            mean_y_row,
            x_table,
            y_table,
        )

    def get_min_max_idx(self, mean_sep, min_txt, mean_txt, max_txt):
        """
        From the extreme and mean coordinates of a textual area, deduce to which columns or rows indices
        the cell might be part of.
        If min_idx and max_idx are the same, it means that the detected textual area corresponds to a real table cell
        If not, it means that the area must be cropped because it extends over several columns/rows
        @param mean_sep: List of mean coordinates of all the table separators (either columns or rows)
        @param min_txt: minimal coordinate of the textual area
        @param mean_txt: middle of the textual area
        @param max_txt: maximal coordinate of the textual area
        """
        # Determination of the minimum and maximum index of each text cell (in the table grid structure),
        # by taking into account not the extreme coordinates of the cells,
        # but a weighted coordinate between the center of the cell and its min/max coordinates
        # (in order not to cut cells than just span a few pixels more than the rows/columns separators)

        weighted_min = (min_txt + mean_txt) / 2
        weighted_max = (max_txt + mean_txt) / 2

        min_idx = bisect(mean_sep, weighted_min)
        max_idx = bisect(mean_sep, weighted_max)

        return min_idx, max_idx

    def cut_cells(
        self,
        txt_areas,
        mean_x_txt,
        mean_y_txt,
        min_x_txt,
        min_y_txt,
        max_x_txt,
        max_y_txt,
        mean_y_row,
        mean_x_col,
        x_table,
        y_table,
    ):
        # for each detected textual area
        for i in range(len(txt_areas)):
            if is_cell_in_table(x_table, y_table, mean_x_txt[i], mean_y_txt[i]):
                w, h = txt_areas[i].size

                min_col_idx, max_col_idx = self.get_min_max_idx(
                    mean_x_col, min_x_txt[i], mean_x_txt[i], max_x_txt[i]
                )
                # min_row_idx, max_row_idx = self.get_min_max_idx(mean_y_row, min_y_txt[i], mean_y_txt[i], max_y_txt[i])

                # if the detected cell contains in fact multiple merged cells, split them
                if min_col_idx < max_col_idx:
                    # for each column index where the detected textual area extends over
                    for col_idx in np.arange(min_col_idx, max_col_idx + 1):
                        if 0 < col_idx < len(mean_x_col):
                            row_idx = bisect(mean_y_row, mean_y_txt[i])

                            left = max(0, mean_x_col[col_idx - 1] - min_x_txt[i])
                            right = (
                                min(mean_x_col[col_idx], max_x_txt[i]) - min_x_txt[i]
                            )
                            sub_area = txt_areas[i].crop((left, 0, right, h))
                            sub_area.save(
                                f"{self.output_path}/({row_idx},{col_idx}).jpg"
                            )

                            x_min = max(mean_x_col[col_idx - 1], min_x_txt[i])
                            x_max = min(mean_x_col[col_idx], max_x_txt[i])
                            coord = (
                                sum([x_min, x_max]) / 2,
                                sum([min_y_txt[i], max_y_txt[i]]) / 2,
                            )
                            self.add_to_output(row_idx, col_idx, coord, sub_area)

                        # # Double loop to cut all the cells that span multiple rows or columns,
                        # # and to save them according to their correct index
                        # # In reality, we don't cut the cells vertically, because astronomical tables cells
                        # # are generally well detected vertically, and trying to cut them according to this axis
                        # # leads to an over-cutting of the cells
                        # for row_idx in np.arange(min_row_idx, max_row_idx + 1):
                        #     if is_in_range(
                        #         col_idx, mean_x_col, row_idx, mean_y_row
                        #     ):
                        #         area = (
                        #             max(0, mean_x_col[col_idx - 1] - min_x_txt[i]),
                        #             max(0, mean_y_row[row_idx - 1] - min_y_txt[i]),
                        #             min(mean_x_col[col_idx], max_x_txt[i]) - min_x_txt[i],
                        #             min(mean_y_row[row_idx], max_y_txt[i]) - min_y_txt[i],
                        #         )
                        #
                        #         x_min = max(
                        #             mean_x_col[col_idx - 1], min_x_txt[i]
                        #         )
                        #         x_max = min(mean_x_col[col_idx], max_x_txt[i])
                        #
                        #         sub_area = txt_areas[i].crop(
                        #             area
                        #         )
                        #         sub_area.save(
                        #             f"{self.output_path}/({row_idx},{col_idx}).jpg"
                        #         )
                        #
                        #         coord = (
                        #             sum([x_min, x_max]) / 2,
                        #             sum([min_y_txt[i], max_y_txt[i]]) / 2,
                        #         )
                        #         self.add_to_output(row_idx, col_idx, coord, sub_area)
                # If the cells don't span multiple rows or columns, we index them according to their center coordinates,
                # in relation to rows and columns separators
                else:
                    col_idx = bisect(mean_x_col, mean_x_txt[i])
                    row_idx = bisect(mean_y_row, mean_y_txt[i])
                    txt_areas[i].save(f"{self.output_path}/({row_idx},{col_idx}).jpg")
                    self.add_to_output(
                        row_idx, col_idx, (mean_x_txt[i], mean_y_txt[i]), txt_areas[i]
                    )

    def add_to_output(self, row_idx, col_idx, coord, img):
        if not str(row_idx) in self.output:
            self.output[str(row_idx)] = {}

        if not str(col_idx) in self.output[str(row_idx)]:
            self.output[str(row_idx)][str(col_idx)] = {}

        self.output[str(row_idx)][str(col_idx)] = {
            "img": f"{row_idx},{col_idx}",
            "coord": coord,
            "dim": img.size,
            "txt": "",
        }

    def predict(self, img):
        """
        Returns the output of the model for the predictions of the four branches corresponding to the different labels
        pred_1 = text related labels
        pred_2 = table related labels
        pred_3 = column separators related labels
        pred_4 = row separators related labels
        @param img:
        @type img: Image.Image
        @return:
        @rtype:
        """
        # Outputs of the model according to the different final branches (and, consequently, the different labels)
        red_img = resize(img, size=self.img_size, keep_aspect_ratio=True)

        inp = np.array(red_img, dtype=np.float32) / 255
        if self.normalize:
            inp = (inp - inp.mean(axis=(0, 1))) / (inp.std(axis=(0, 1)) + 10 ** -7)
        inp = (
            torch.from_numpy(inp.transpose(2, 0, 1)).float().to(self.device)
        )  # HWC -> CHW tensor
        with torch.no_grad():
            outputs = self.model(inp.reshape(1, *inp.shape))
            pred_1 = outputs[0][0].max(0)[1].cpu().numpy()
            pred_2 = outputs[1][0].max(0)[1].cpu().numpy()
            pred_3 = outputs[2][0].max(0)[1].cpu().numpy()
            pred_4 = outputs[3][0].max(0)[1].cpu().numpy()

        return pred_1, pred_2, pred_3, pred_4

    def find_contour_areas(self, pred, img, label, restr_labels):
        cnt_areas = []
        # NOTE find_contours() fails and the script stops
        for cnt in self.find_contours(pred, img, label, restr_labels):
            cnt_areas.append(cv2.contourArea(cnt))
        return cnt_areas

    def find_contours(self, pred, img, label, restr_labels="restr_labels_2"):
        """
        @param pred:
        @type pred:
        @param img:
        @type img: Image.Image
        @param label:
        @type label:
        @param restr_labels:
        @type restr_labels:
        @return: List of contours
        """
        label = getattr(self, restr_labels).index(label) + 1

        mask_pred = cv2.resize(
            (pred == label).astype(np.uint8),
            img.size,
            interpolation=cv2.INTER_NEAREST,
        )

        _, contours, _ = cv2.findContours( # NOTE ⚠️⚠️⚠️ THE SCRIPT STOPS AT THIS STAGE AND FAILS ⚠️⚠️⚠️
            mask_pred, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
        )

        return contours

    def straighten_img(self, img: Image):
        """
        Detect table and column areas in order to rotate the image to align to columns separators
        @param img: image on which perform table and column detection as well as rotation
        @type img: Image.Image
        @return: img
        @rtype: Image
        """
        img = img.convert("RGB") # convert to accepted format
        # We first compute only the prediction of the second and third branch,
        # which correspond, in our network with four branches, to the table and column separators labels
        _, pred_2, pred_3, _ = self.predict(img)

        # NOTE find_contours() fails and the script stops
        cnt_areas = self.find_contour_areas(pred_2, img, TABLE_LABEL, "restr_labels_2")
        # If there is at least one image zone where a table was detected,
        # and the proportion of the largest area of the detected zones to the image
        # is superior to the defined AREA_RATIO_THRESHOLD,
        # it means that the table area detected is probably not one
        if not (
            len(cnt_areas) >= 1
            and check_area_to_img_ratio(img, max(cnt_areas), TABLE_LABEL)
        ):
            return None

        # Rotation of the table according to the mean angle of all the columns separators
        img = self.rotate_img(img, pred_3, COLUMN_LABEL)
        img.save(f"{self.output_path}/rotated_{self.idx}.jpg")
        return img

    def rotate_img(self, img, pred, label=COLUMN_LABEL):
        """
        @param img:
        @type img: Image.Image
        @param pred:
        @type pred:
        @param label:
        @type label:
        @return: rotated image
        @rtype: Image.Image
        """
        angles = []
        # for each detected column separator, compute the angle that would be needed for the image to be straight according to it
        # NOTE find_contours() fails and the script stops
        for cnt in self.find_contours(pred, img, label, "restr_labels_3"):
            if check_area_to_img_ratio(img, cv2.contourArea(cnt), label):
                rect = cv2.minAreaRect(cnt)
                width, height, rotation = (
                    int(rect[1][0]),
                    int(rect[1][1]),
                    rect[-1],
                )
                if label == COLUMN_LABEL:
                    if rotation < -45:
                        angle = 90 + rotation
                    else:
                        angle = rotation
                    angles.append(angle)

        if len(angles) == 0:
            angles = [0]

        # rotate the image according to the mean of the angles
        return img.rotate(statistics.mean(angles))

    def draw_areas(self, img, pred, label, annotated_img, restr_label="restr_labels_1"):
        """
        Annotation function
        @param restr_label:
        @type restr_label:
        @param img:
        @type img: Image.Image
        @param pred:
        @type pred:
        @param label:
        @type label:
        @param annotated_img:
        @type annotated_img:
        @rtype: areas: List[Image.Image] > list of images of the detected areas
                x_coord: List[array(dtype=int32)] > list of lists of the x coordinates for each detected area
                y_coord: List[array(dtype=int32)] > list of lists of the y coordinates for each detected area
        """
        areas = []
        y_coord = []
        x_coord = []
        margin = ADDITIONAL_MARGIN_RATIO[label]
        # NOTE find_contours() fails and the script stops
        for cnt in self.find_contours(pred, img, label, restr_label):
            self.draw_annotation(
                annotated_img,
                line=list(map(tuple, cnt.reshape(-1, 2).tolist())) + cnt[0][0].tolist(),
                fill=LABEL_TO_COLOR_MAPPING[label],
                width=2,
            )

            if check_area_to_img_ratio(img, cv2.contourArea(cnt), label):
                margins = np.array([0, 0, 0, 0], dtype=np.int32)
                # for rectangular extracted areas (with right angles)
                if self.straight_bbox:
                    x, y, width, height = cv2.boundingRect(cnt)
                    if self.add_margin:
                        m = int(min(margin * width, margin * height))
                        bbox = np.asarray(
                            [
                                [x - m, y - m],
                                [x + width + m, y - m],
                                [x + width + m, y + height + m],
                                [x - m, y + height + m],
                            ]
                        )
                        bbox = np.clip(bbox, a_min=(0, 0), a_max=img.size)
                        margins = np.array(
                            [
                                min(m, x),
                                min(m, y),
                                -min(img.size[0] - x - width, m),
                                -min(img.size[1] - y - height, m),
                            ],
                            dtype=np.int32,
                        )
                    else:
                        bbox = np.asarray(
                            [
                                [x, y],
                                [x + width, y],
                                [x + width, y + height],
                                [x, y + height],
                            ]
                        )
                    result_img = img.crop(tuple(bbox[0]) + tuple(bbox[2]))
                # for diamond-shaped extracted areas (without right angles)
                else:
                    rect = cv2.minAreaRect(cnt)
                    width, height, angle = (
                        int(rect[1][0]),
                        int(rect[1][1]),
                        rect[-1],
                    )

                    if self.add_margin:
                        m = int(min(margin * width, margin * height))
                        width, height = width + 2 * m, height + 2 * m
                        rect = (rect[0], (width, height), angle)
                        margins = np.array([m, m, -m, -m], dtype=np.int32)

                    bbox = np.int32(cv2.boxPoints(rect))
                    dst_pts = np.array(
                        [
                            [0, height - 1],
                            [0, 0],
                            [width - 1, 0],
                            [width - 1, height - 1],
                        ],
                        dtype=np.float32,
                    )
                    M = cv2.getPerspectiveTransform(bbox.astype(np.float32), dst_pts)
                    result_img = Image.fromarray(
                        cv2.warpPerspective(np.array(img), M, (width, height))
                    )

                    if angle < -45:
                        result_img = result_img.transpose(Image.ROTATE_90)

                if self.draw_margin:
                    width, height = result_img.size
                    lw = int(min([0.01 * width, 0.01 * height]))
                    draw = ImageDraw.Draw(result_img)
                    rect = np.array([0, 0, width, height], dtype=np.int32) + margins
                    draw.rectangle(
                        rect.tolist(), fill=None, outline=MARGIN_COLOR, width=lw
                    )

                areas.append(result_img)
                y_coord.append(bbox[:, 1])
                x_coord.append(bbox[:, 0])

                self.save_annotated_img(annotated_img)

        return areas, x_coord, y_coord

    def transcribe(self, output=None):
        """
        @param output: Dictionary containing information on the detected cell images
        @return output: Dictionary containing information on the detected cell images and their transcription
        """
        images = None
        if output:
            self.output = output["table"]
            images = output["imgs"]

        params = BaseOptions().parser()

        htr_model = RCNN(
            imheight=params.imgH,
            nc=params.NC,
            n_conv_layers=params.N_CONV_LAYERS,
            n_conv_out=params.N_CONV_OUT,
            conv=params.CONV,
            batch_norm=params.BATCH_NORM,
            max_pool=params.MAX_POOL,
            n_r_layers=params.N_REC_LAYERS,
            n_r_input=params.N_REC_INPUT,
            n_hidden=params.N_HIDDEN,
            n_out=len(params.alphabet),
            bidirectional=params.BIDIRECTIONAL,
            feat_extractor=params.feat_extractor,
            dropout=params.DROPOUT,
        )

        if params.cuda and torch.cuda.is_available():
            htr_model.load_state_dict(torch.load(HTR_MODEL))
            htr_model = htr_model.cuda()
        else:
            htr_model.load_state_dict(
                torch.load(
                    HTR_MODEL, map_location=torch.device("cpu")
                )  # todo: use CUDA
            )
        transcribed = transcribe(
            htr_model,
            self.output_path,
            filenames=images,
            imgH=params.imgH,
            imgW=params.imgW,
        )

        for img in transcribed:
            row_idx, col_idx = img.split(",")
            if str(row_idx) in self.output:
                if str(col_idx) in self.output[str(row_idx)]:
                    self.output[str(row_idx)][str(col_idx)]["txt"] = transcribed[img]

        return self.output
