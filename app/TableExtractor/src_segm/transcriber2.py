import argparse
import cv2
from PIL import Image, ImageDraw

import numpy as np
import torch

import re
from bisect import bisect

import shutil
from pathlib import Path

from app.TableExtractor.src_segm.extractor import (
    AREA_RATIO_THRESHOLD,
)
from app.TableExtractor.src_segm.utils import VALID_EXTENSIONS
from models import load_model_from_path
from app.utils.utils import (
    check_path,
    create_dir,
    get_files_from_dir,
)
from app.TableExtractor.src_segm.utils.const import (
    BACKGROUND_LABEL,
    ILLUSTRATION_LABEL,
    TEXT_LABEL,
    LABEL_TO_COLOR_MAPPING,
    MODEL_FILE,
    TABLE_LABEL,
    BASELINE_LABEL,
    LINE_LABEL,
    COLUMN_LABEL,
    ADDITIONAL_MARGIN_RATIO,
)
from utils.image import Pdf2Image, resize, get_images
from utils.logger import get_logger, print_info, print_error
from utils.path import MODELS_PATH
import statistics

from numpy import asarray

import os


class TableTranscriber:
    """
    Extract and transcribe tables from files in a given input_dir folder and save them in the provided output_dir.
    Supported input extensions are: jpg, png, tiff, pdf.
    Each input image is thought to contain 0 or 1 table (no more than 1).
    This program is thought to work on a four branches network, with the following labels:
    restricted_labels_1 = [1, 4, 6], restricted_labels_2 = [9], restricted_labels_3 = [14], restricted_labels_4 = [13]
    """

    def __init__(
        self,
        input_dir,
        output_dir,
        labels_to_extract=None,
        in_ext=VALID_EXTENSIONS,
        out_ext="jpg",
        tag="four_branches_lines_columns",
        save_annotations=True,
        straight_bbox=False,
        add_margin=True,
        draw_margin=False,
    ):

        self.input_dir = check_path(input_dir).absolute()  # note
        self.files = get_files_from_dir(
            self.input_dir, valid_extensions=in_ext, recursive=True, sort=True  # note
        )
        self.output_dir = create_dir(output_dir).absolute()  # note
        self.out_extension = out_ext
        model_path = check_path(MODELS_PATH / tag / MODEL_FILE)
        self.device = torch.device(
            "cuda" if torch.cuda.is_available() else "cpu"
        )  # note

        self.model, (  # note
            self.img_size,
            restricted_labels_1,
            restricted_labels_2,
            restricted_labels_3,
            restricted_labels_4,
            self.normalize,
        ) = load_model_from_path(
            model_path,
            device=self.device,
            attributes_to_return=[
                "train_resolution",
                "restricted_labels_1",
                "restricted_labels_2",
                "restricted_labels_3",
                "restricted_labels_4",
                "normalize",
            ],
        )

        self.model.eval()  # note

        (
            self.restricted_labels_1,  # note
            self.restricted_labels_2,  # note
            self.restricted_labels_3,  # note
            self.restricted_labels_4,  # note
        ) = (
            sorted(restricted_labels_1),
            sorted(restricted_labels_2),
            sorted(restricted_labels_3),
            sorted(restricted_labels_4),
        )
        self.restricted_labels = sorted(
            restricted_labels_1
            + restricted_labels_2
            + restricted_labels_3
            + restricted_labels_4
        )

        self.labels_to_extract = (
            [ILLUSTRATION_LABEL, TEXT_LABEL]
            if labels_to_extract is None
            else sorted(labels_to_extract)
        )
        if not set(self.labels_to_extract).issubset(self.restricted_labels):
            raise ValueError(
                "Incompatible `labels_to_extract` and `tag` arguments: "
                f"model was trained using {self.restricted_labels} labels only"
            )

        self.save_annotations = save_annotations  # note
        self.straight_bbox = straight_bbox  # note
        self.add_margin = add_margin  # note
        self.draw_margin = add_margin and draw_margin  # note

    def run(self):
        for filename in self.files:  # note
            try:
                imgs_with_names = get_images(filename)  # note
            except (NotImplementedError, OSError):
                imgs_with_names = []  # note

            for img, name in imgs_with_names:
                # We first compute only the prediction of the second and third branch, which correspond, in our network four_branches_tables, to the table and column separators labels
                _, pred_2, pred_3, _ = self.predict(img)  # note

                pred = pred_2  # note
                label = TABLE_LABEL  # note
                label_idx = self.restricted_labels_2.index(label) + 1  # note

                mask_pred = cv2.resize(
                    (pred == label_idx).astype(np.uint8),  # note
                    img.size,  # note
                    interpolation=cv2.INTER_NEAREST,  # note
                )
                _, contours, _ = cv2.findContours(  # note
                    mask_pred, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE  # note
                )
                cnt_areas = []  # note

                for cnt in contours:  # note
                    cnt_areas.append(cv2.contourArea(cnt))  # note
                # If there is a table, with sufficient area, then:
                if (  # note
                    len(cnt_areas) >= 1
                    and max(cnt_areas) / (img.size[0] * img.size[1])
                    >= AREA_RATIO_THRESHOLD[label]
                ):
                    # Rotation of the table according to the mean angle of all the columns separators
                    angle = self.rotate_image(img, pred_3, COLUMN_LABEL)  # note
                    img = img.rotate(angle)  # note

                    # After the rotation of the image, prediction of all the labels
                    pred_1, pred_2, pred_3, pred_4 = self.predict(img)  # note
                    img_with_annotations = img.copy()  # note

                    img_w, img_h = img.size

                    # Extraction of all the different labels, with their corresponding coordinates (in order to recognize the structure of the table)
                    for label in self.labels_to_extract:  # note
                        if label != BACKGROUND_LABEL:  # note
                            if label in self.restricted_labels_1:  # note
                                if label == TEXT_LABEL:  # note
                                    # For the text label, determination of the mean, and extreme coordinates of all the textual cells
                                    (
                                        text_extracted_elements,
                                        x_text,
                                        y_text,
                                    ) = self.extract(
                                        img, pred_1, label, img_with_annotations  # note
                                    )
                                    x_t_min = [np.min(x) for x in x_text]  # note
                                    y_t_min = [np.min(y) for y in y_text]  # note
                                    x_t_max = [np.max(x) for x in x_text]  # note
                                    y_t_max = [np.max(y) for y in y_text]  # note
                                    x_text = [np.mean(x) for x in x_text]  # note
                                    y_text = [np.mean(y) for y in y_text]  # note

                                else:  # note
                                    extracted_elements, _, _ = self.extract(
                                        img, pred_1, label, img_with_annotations  # note
                                    )

                            elif label in self.restricted_labels_2:  # note
                                # Extraction of the tables
                                extracted_elements, x_table, y_table = self.extract(
                                    img, pred_2, label, img_with_annotations  # note
                                )
                            elif label in self.restricted_labels_3:  # note
                                # Extraction of the columns separators and determination of their coordinates
                                extracted_elements, x_coord, y_coord = self.extract(
                                    img, pred_3, label, img_with_annotations  # note
                                )
                                x_coord = [np.mean(x) for x in x_coord]
                                # Deletion of too close separators
                                x_columns = self.delete_close(
                                    list(dict.fromkeys(np.sort(x_coord))),
                                    img_w / 150,  # note
                                )
                            elif label in self.restricted_labels_4:
                                # Extraction of the lines separators and determination of their coordinates
                                extracted_elements, x_coord, y_coord = self.extract(
                                    img, pred_4, label, img_with_annotations
                                )
                                y_coord = [np.mean(y) for y in y_coord]
                                # Deletion of too close separators
                                y_lines = self.delete_close(
                                    list(dict.fromkeys(np.sort(y_coord))), img_h / 100
                                )

                    # Draw the grid separators on the annotated image
                    draw = ImageDraw.Draw(img_with_annotations)  # note
                    for y in y_lines:
                        draw.line([(50, y), (100, y)], fill=255, width=10)  # note
                    for x in x_columns:
                        draw.line([(x, 50), (x, 100)], fill=255, width=10)  # note

                    if self.save_annotations:  # note
                        (self.output_dir / "annotation").mkdir(exist_ok=True)
                        img_with_annotations.save(
                            self.output_dir
                            / "annotation"
                            / "{}_annotated.{}".format(name, self.out_extension)
                        )

                    self.create_tables(  # note
                        text_extracted_elements,
                        x_text,
                        y_text,
                        x_t_min,
                        y_t_min,
                        x_t_max,
                        y_t_max,
                        y_lines,
                        x_columns,
                        x_table,
                        y_table,
                        self.output_dir / f"table_{name}",
                        img.size,
                        name,
                    )

    # Delete elements in list (for columns and rows separators) that are closer than a given threshold
    def delete_close(self, coords, thresh):
        for i in range(len(coords) - 1):
            if coords[i + 1] - coords[i] < thresh:
                coords[i + 1] = coords[i]
        return list(dict.fromkeys(coords))

    # Recognition of the table structure (determination of the index of each cell), post processing (cutting cells than span multiple columns), and creation of .html / .xsv tables transcriptions outputs
    def create_tables(
        self,
        text_extracted_elements,
        x_text,
        y_text,
        x_t_min,
        y_t_min,  # note
        x_t_max,
        y_t_max,  # note
        y_lines,
        x_columns,
        x_table,
        y_table,
        output_path,
        img_size,
        img_name,
    ):

        if output_path.exists():
            shutil.rmtree(output_path)
        output_path = create_dir(output_path)

        x_indices = {}
        y_indices = {}

        for i in range(len(text_extracted_elements)):

            if (
                np.array(x_table).min() <= x_text[i] <= np.array(x_table).max()  # note
                and np.array(y_table).min()
                <= y_text[i]
                <= np.array(y_table).max()  # note
            ):
                w, h = text_extracted_elements[i].size
                # Determination of the minimum and maximum index of each text cell (in the table grid structure), by taking into account not the extreme coordinates of the cells, but a ponderate coordinate between the center of the cell and its min/max coordinates (in order not to cut cells than just span a few pixels more than the rows/columns separators)
                x_t_min_pond = (x_t_min[i] + x_text[i]) / 2  # note
                x_t_max_pond = (x_t_max[i] + x_text[i]) / 2  # note

                y_t_min_pond = (y_t_min[i] + y_text[i]) / 2  # note
                y_t_max_pond = (y_t_max[i] + y_text[i]) / 2  # note

                c_min_index = bisect(x_columns, x_t_min_pond)  # note
                c_max_index = bisect(x_columns, x_t_max_pond)  # note

                l_min_index = bisect(y_lines, y_t_min_pond)  # note
                l_max_index = bisect(y_lines, y_t_max_pond)  # note

                (output_path / "cell_images").mkdir(exist_ok=True)

                # Double loop to cut all the cells that span multiple rows or columns, and to save them according to their correct index
                # In reality, we don't cut the cells vertically, because astronomical tables cells are generally well detected vertically, and trying to cut them according to this axis leads to an over-cutting of the cells
                if c_min_index < c_max_index or l_min_index < l_max_index:  # note
                    for c_index in np.arange(c_min_index, c_max_index + 1):  # note
                        for l_index in np.arange(l_min_index, l_max_index + 1):  # note
                            if 0 < c_index < len(
                                x_columns
                            ) and 0 < l_index < len(  # note
                                y_lines  # note
                            ):  # note
                                area = (
                                    max(0, x_columns[c_index - 1] - x_t_min[i]),
                                    0,
                                    min(x_columns[c_index], x_t_max[i]) - x_t_min[i],
                                    h,
                                )

                                x_min = max(x_columns[c_index - 1], x_t_min[i])  # note
                                x_max = min(x_columns[c_index], x_t_max[i])  # note

                                sub_element = text_extracted_elements[i].crop(
                                    area
                                )  # note
                                index = [l_index, c_index]
                                sub_element.save(  # note
                                    output_path
                                    / "cell_images"
                                    / f"({index[0]},{index[1]}).jpg"
                                )
                                x_indices[f"{index[0]},{index[1]}"] = [
                                    x_min,
                                    x_max,
                                ]
                                y_indices[f"{index[0]},{index[1]}"] = [
                                    y_t_min[i],
                                    y_t_max[i],
                                ]
                # If the cells don't span multiple rows or columns, we index them according to their center coordinates, in relation to rows and columns separators
                else:
                    c_index = bisect(x_columns, x_text[i])
                    l_index = bisect(y_lines, y_text[i])
                    index = [l_index, c_index]
                    x_indices["{},{}".format(index[0], index[1])] = [
                        x_t_min[i],
                        x_t_max[i],
                    ]
                    y_indices["{},{}".format(index[0], index[1])] = [
                        y_t_min[i],
                        y_t_max[i],
                    ]
                    text_extracted_elements[i].save(
                        output_path
                        / "cell_images"
                        / "{}.{}".format("({0},{1})".format(index[0], index[1]), "jpg")
                    )

        path = output_path / "cell_images"
        # Call of the HTR line prediction model to transcribe all the cells
        os.system(
            "CUDA_VISIBLE_DEVICES=0 python HTR/line_predictor.py --data_path {} --model_path HTR/trained_networks/medieval_numbers.pth --imgh 64".format(
                path
            )
        )

        # Creation of the corresponding HTML and XML files
        fileout_html = open(output_path / "html_table_{}.html".format(img_name), "w")
        fileout_xml = open(output_path / "xml_transcript_{}.xml".format(img_name), "w")

        xml = ""
        xml += """<?xml version="1.0" encoding="UTF-8"?>
        <alto xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xmlns="http://www.loc.gov/standards/alto/ns-v4#"
          xsi:schemaLocation="http://www.loc.gov/standards/alto/ns-v4# http://www.loc.gov/standards/alto/v4/alto-4-2.xsd">
          <Description>
            <MeasurementUnit>pixel</MeasurementUnit>
            <sourceImageInformation>
              <fileName></fileName>
          </sourceImageInformation>
        </Description>
        <Layout>
        <Page WIDTH="{0}"
              HEIGHT="{1}"
              PHYSICAL_IMG_NR="0"
              ID="eSc_dummypage_">
        <PrintSpace HPOS="0"
              VPOS="0"
              WIDTH="{0}"
              HEIGHT="{1}">
        <TextBlock ID="eSc_dummyblock_">\n""".format(
            img_size[0], img_size[1]
        )

        html_table = f"<table border='1' id='{img_name}'>\n"
        for line in range(len(y_lines) + 1):
            html_table += "<tr>\n"
            for column in range(len(x_columns) + 1):
                file_name = (
                    output_path
                    / "cell_images"
                    / "{}.{}".format("({0},{1})".format(line, column), "jpg")
                )
                if Path(file_name).is_file():
                    [x_min, x_max] = x_indices["{},{}".format(line, column)]
                    [y_min, y_max] = y_indices["{},{}".format(line, column)]

                    transcript = (
                        [
                            line_file
                            for line_file in open(path / "predictions.txt")
                            if "({},{})".format(line, column) in line_file
                        ][0]
                        .strip()
                        .split()
                    )
                    if len(transcript) >= 3:
                        transcript = transcript[2:]
                        transcript = [
                            [int(s) for s in trans.split() if s.isdigit()]
                            for trans in transcript
                        ]
                    else:
                        transcript = ""
                    html_table += "<td><img src='{0}' alt='Cell'><input type='text' id='cell' value ='{1}' width='30px' size ='1'></td>\n".format(
                        file_name,
                        ", ".join(map(str, [trans[0] for trans in transcript])),
                    )

                    xml += '<TextLine ID=""\n'
                    xml += 'BASELINE="{} {} {} {}"\n'.format(x_min, y_max, x_max, y_max)
                    xml += 'HPOS="{}"\n'.format(x_min)
                    xml += 'VPOS="{}"\n'.format(y_min)
                    xml += 'WIDTH="{}"\n'.format(x_max - x_min)
                    xml += 'HEIGHT="{}">\n'.format(y_max - y_min)
                    xml += '<Shape><Polygon POINTS="{} {} {} {} {} {} {} {}"/></Shape>\n'.format(
                        x_min, y_max, x_min, y_min, x_max, y_min, x_max, y_max
                    )
                    xml += '<String CONTENT="{}"\n'.format(
                        ", ".join(map(str, [trans[0] for trans in transcript]))
                    )
                    xml += 'HPOS="{}"\n'.format(x_min)
                    xml += 'VPOS="{}"\n'.format(x_min)
                    xml += 'WIDTH="{}"\n'.format(x_max - x_min)
                    xml += 'HEIGHT="{}"></String>\n'.format(y_max - y_min)
                    xml += "</TextLine>\n"

                else:
                    html_table += "<td><input type='text' id='cell' width='30px' size ='1'></td>\n"
            html_table += "</tr>\n"
        html_table += "</table>"
        xml += "</TextBlock>\n </PrintSpace>\n </Page>\n </Layout>\n </alto>"

        csv_fct = (
            "const table=document.getElementById('"
            + img_name
            + "');let csv='';for(let i=0,row;row=table.rows[i];i++){for(let j=0,col;col=row.cells[j];j++){for(let k=0;k<col.children.length;k++){if(col.children[k].tagName==='INPUT'){csv+=col.children[k].value+','} } }csv+='\\n'}const blob=new Blob([csv],{type:'text/csv;charset=utf-8;'});if(navigator.msSaveBlob){navigator.msSaveBlob(blob,'table.csv')}else{const link=document.createElement('a');if(link.download!==undefined){const url=window.URL.createObjectURL(blob);link.setAttribute('href',url);link.setAttribute('download','table.csv');link.style.visibility='hidden';document.body.appendChild(link);link.click();document.body.removeChild(link)} }"
        )
        html_table += '<button onclick="' + csv_fct + '">Export to CSV</button>'

        xml_fct = (
            "const blob=new Blob([`"
            + xml
            + '`],{type:"text/xml;charset=utf-8;"});if(navigator.msSaveBlob){navigator.msSaveBlob(blob,"table.xml")}else{const link=document.createElement("a");if(link.download!==undefined){const url=window.URL.createObjectURL(blob);link.setAttribute("href",url);link.setAttribute("download","table.xml");link.style.visibility="hidden";document.body.appendChild(link);link.click();document.body.removeChild(link)} }'
        )
        html_table += "<button onclick='" + xml_fct + "'>Export to XML</button>"

        export_fct = (
            "const table=document.getElementById('"
            + img_name
            + "');let txt='';for(let i=0,row; row=table.rows[i]; i++){for(let j=0,col; col=row.cells[j]; j++){for(let k=0;k<col.children.length;k++){if(col.children[k].tagName==='IMG'){txt+=col.children[k].src+' '}if(col.children[k].tagName==='INPUT'){txt+=col.children[k].value+'\\n'}}}}const blob=new Blob([txt],{type:'text/txt;charset=utf-8;'});if(navigator.msSaveBlob){navigator.msSaveBlob(blob,'table.txt')}else{const link=document.createElement('a');if(link.download!==undefined){const url=window.URL.createObjectURL(blob);link.setAttribute('href',url);link.setAttribute('download','table.txt');link.style.visibility='hidden';document.body.appendChild(link);link.click();document.body.removeChild(link)}}"
        )
        html_table += (
            '<button onclick="' + export_fct + '">Export to training data</button>'
        )

        fileout_html.writelines(html_table)
        fileout_html.close()

        fileout_xml.writelines(xml)
        fileout_xml.close()

    def predict(self, image):
        red_img = resize(image, size=self.img_size, keep_aspect_ratio=True)  # note
        inp = np.array(red_img, dtype=np.float32) / 255  # note
        if self.normalize:  # note
            inp = (inp - inp.mean(axis=(0, 1))) / (
                inp.std(axis=(0, 1)) + 10 ** -7
            )  # note
        inp = (
            torch.from_numpy(inp.transpose(2, 0, 1)).float().to(self.device)
        )  # HWC -> CHW tensor  # note
        with torch.no_grad():  # note
            outputs = self.model(inp.reshape(1, *inp.shape))
            pred_1 = outputs[0][0].max(0)[1].cpu().numpy()
            pred_2 = outputs[1][0].max(0)[1].cpu().numpy()
            pred_3 = outputs[2][0].max(0)[1].cpu().numpy()
            pred_4 = outputs[3][0].max(0)[1].cpu().numpy()

        return pred_1, pred_2, pred_3, pred_4  # note

    # Rotation of the image depending on the mean angle of all the column separators (corresponding to restricted_labels_3) of the table in the image
    def rotate_image(self, image, pred, label):
        label_idx = self.restricted_labels_3.index(label) + 1  # note

        rotate_angle = 0
        total_rotate = []  # note

        mask_pred = cv2.resize(
            (pred == label_idx).astype(np.uint8),  # note
            image.size,  # note
            interpolation=cv2.INTER_NEAREST,  # note
        )
        _, contours, _ = cv2.findContours(
            mask_pred, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE  # note
        )

        for cnt in contours:  # note
            cnt_area = cv2.contourArea(cnt)  # note

            if (
                cnt_area
                / (
                    image.size[0] * image.size[1]
                )  # note: peut-être ici dans check_img_dim il y a des parentaises en trop ?
                >= AREA_RATIO_THRESHOLD[label]  # note
            ):
                rect = cv2.minAreaRect(cnt)  # note
                width, height, angle = (
                    int(rect[1][0]),
                    int(rect[1][1]),
                    rect[-1],
                )  # note
                if label == COLUMN_LABEL:  # note
                    if angle < -45:  # note
                        rotate_angle = 90 + angle  # note
                    else:  # note
                        rotate_angle = angle  # note

                    total_rotate.append(rotate_angle)  # note

        rotate_angle = statistics.mean(total_rotate)
        print("Rotation angle:" + str(rotate_angle))
        return rotate_angle  # note

    # Annotation functions
    def extract(self, image, pred, label, image_with_annotations):
        if label in self.restricted_labels_1:  # note
            label_idx = self.restricted_labels_1.index(label) + 1
        elif label in self.restricted_labels_2:  # note
            label_idx = self.restricted_labels_2.index(label) + 1
        elif label in self.restricted_labels_3:  # note
            label_idx = self.restricted_labels_3.index(label) + 1
        else:  # note
            label_idx = self.restricted_labels_4.index(label) + 1
        color = LABEL_TO_COLOR_MAPPING[label]  # note

        mask_pred = cv2.resize(
            (pred == label_idx).astype(np.uint8),
            image.size,
            interpolation=cv2.INTER_NEAREST,
        )
        _, contours, _ = cv2.findContours(
            mask_pred, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE
        )

        results = []  # note
        y_coord = []  # note
        x_coord = []  # note
        y_coord_bis = []
        i = 0
        for cnt in contours:  # note

            cnt_area = cv2.contourArea(cnt)  # note
            if self.save_annotations:  # note
                draw = ImageDraw.Draw(image_with_annotations)  # note
                draw.line(
                    list(map(tuple, cnt.reshape(-1, 2).tolist()))
                    + cnt[0][0].tolist(),  # note
                    fill=color,  # note
                    width=2,  # note
                )

            if (
                cnt_area / (image.size[0] * image.size[1])
                >= AREA_RATIO_THRESHOLD[label]  # note
            ):
                if self.straight_bbox:  # note
                    x, y, width, height = cv2.boundingRect(cnt)  # note
                    if self.add_margin:  # note
                        m = int(
                            min(
                                ADDITIONAL_MARGIN_RATIO[label] * width,  # note
                                ADDITIONAL_MARGIN_RATIO[label] * height,  # note
                            )
                        )
                        bbox = np.asarray(  # note
                            [
                                [x - m, y - m],
                                [x + width + m, y - m],
                                [x + width + m, y + height + m],
                                [x - m, y + height + m],
                            ]
                        )
                        bbox = np.clip(bbox, a_min=(0, 0), a_max=image.size)  # note
                        margins = np.array(  # note
                            [
                                min(m, x),
                                min(m, y),
                                -min(image.size[0] - x - width, m),
                                -min(image.size[1] - y - height, m),
                            ],
                            dtype=np.int32,  # note
                        )
                    else:  # note
                        bbox = np.asarray(  # note
                            [
                                [x, y],
                                [x + width, y],
                                [x + width, y + height],
                                [x, y + height],
                            ]
                        )
                    result_img = image.crop(tuple(bbox[0]) + tuple(bbox[2]))

                else:  # note
                    rect = cv2.minAreaRect(cnt)  # note
                    width, height, angle = (
                        int(rect[1][0]),
                        int(rect[1][1]),
                        rect[-1],
                    )  # note

                    if self.add_margin:  # note
                        m = int(
                            min(
                                ADDITIONAL_MARGIN_RATIO[label] * width,  # note
                                ADDITIONAL_MARGIN_RATIO[label] * height,  # note
                            )
                        )
                        width, height = width + 2 * m, height + 2 * m  # note
                        rect = (rect[0], (width, height), angle)  # note
                        margins = np.array([m, m, -m, -m], dtype=np.int32)  # note
                    bbox = np.int32(cv2.boxPoints(rect))  # note
                    dst_pts = np.array(
                        [
                            [0, height - 1],  # note
                            [0, 0],  # note
                            [width - 1, 0],  # note
                            [width - 1, height - 1],  # note
                        ],
                        dtype=np.float32,  # note
                    )
                    M = cv2.getPerspectiveTransform(
                        bbox.astype(np.float32), dst_pts
                    )  # note
                    result_img = Image.fromarray(
                        cv2.warpPerspective(np.array(image), M, (width, height))  # note
                    )

                    if angle < -45:
                        result_img = result_img.transpose(Image.ROTATE_90)  # note

                if self.draw_margin:  # note
                    width, height = result_img.size  # note
                    lw = int(min([0.01 * width, 0.01 * height]))  # note
                    draw = ImageDraw.Draw(result_img)  # note
                    rect = (
                        np.array([0, 0, width, height], dtype=np.int32) + margins
                    )  # note
                    draw.rectangle(
                        rect.tolist(),
                        fill=None,
                        outline=(59, 178, 226),
                        width=lw,  # note
                    )

                results.append(result_img)  # note
                y_coord.append(bbox[:, 1])  # note
                x_coord.append(bbox[:, 0])  # note

                if self.save_annotations:
                    lw = int(min([0.005 * image.size[0], 0.005 * image.size[1]]))
                    draw = ImageDraw.Draw(image_with_annotations)
                    # draw.line(list(map(tuple, bbox.tolist())) + [tuple(bbox[0])], fill=(0, 255, 0), width=lw)

        return results, x_coord, y_coord


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Transcribe images of tables into numerical tables"
    )
    parser.add_argument(
        "-i", "--input_dir", nargs="?", type=str, required=True, help="Input directory"
    )
    parser.add_argument(
        "-o",
        "--output_dir",
        nargs="?",
        type=str,
        required=True,
        help="Output directory",
    )
    parser.add_argument(
        "-t",
        "--tag",
        nargs="?",
        type=str,
        default="four_branches_tables",
        help="Model tag to use",
    )
    parser.add_argument(
        "-l",
        "--labels",
        nargs="+",
        type=int,
        default=[ILLUSTRATION_LABEL, TEXT_LABEL, TABLE_LABEL, LINE_LABEL, COLUMN_LABEL],
        help="Labels to extract",
    )
    parser.add_argument(
        "-s", "--save_annot", action="store_true", help="Whether to save annotations"
    )
    parser.add_argument(
        "-sb",
        "--straight_bbox",
        action="store_true",
        help="Use straight bounding box only to"
        "fit connected components found, instead of rotated ones",
    )
    parser.add_argument(
        "-dm",
        "--draw_margin",
        action="store_true",
        help="Draw the margins added, for visual purposes",
    )
    args = parser.parse_args()

    input_dir = check_path(args.input_dir)
    transcribe = TableTranscriber(
        input_dir,
        args.output_dir,
        labels_to_extract=args.labels,
        tag=args.tag,
        save_annotations=args.save_annot,
        straight_bbox=args.straight_bbox,
        draw_margin=args.draw_margin,
    )
    transcribe.run()
