import os
from pathlib import Path
from warnings import warn

APP_NAME = "Table Transcriber"

SECRET_KEY = "SECRET KEY!"
ROOT_DIR = Path(
    os.path.dirname(os.path.abspath(__file__)).split("table-transcriber", 1)[0]
    + "table-transcriber"
)
APP_DIR = Path(__file__).parent.parent
TEMPLATE_DIR = APP_DIR / "templates/"
STATIC_DIR = APP_DIR / "static/"
UPLOAD_DIR = STATIC_DIR / "uploads/"
OUTPUT_DIR = STATIC_DIR / "outputs/"

API_ROUTE = "/api"

if SECRET_KEY == "SECRET KEY!":
    warn("Key hasn't been changed, you should do it", Warning)


class _DEVELOPMENT:
    SECRET_KEY = SECRET_KEY


class _PRODUCTION:
    SECRET_KEY = "IamS3CR37"


CONFIG = {"dev": _DEVELOPMENT, "prod": _PRODUCTION}
