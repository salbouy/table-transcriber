import gc
import json
import os
import shutil
from pathlib import Path
import re

import torch
from PIL import Image
from dotenv import variables


def create_if_not(file_path):
    if not os.path.exists(file_path):
        file = open(file_path, "x")
        file.close()
    return file_path


def get_json_content(filepath):
    with open(filepath) as outfile:
        content = json.load(outfile)
    return content


def empty_dir(path):
    for filename in os.listdir(path):
        file_path = os.path.join(path, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print(f"Failed to delete {file_path}. Reason: {e}")
    return path


def check_path(path):
    path = Path(path)
    if not path.exists():
        raise FileNotFoundError(f"{path.absolute()} does not exist")
    return path


def create_dir(path):
    path = Path(path)
    path.mkdir(parents=True, exist_ok=True)
    return path


def is_last_iteration(iterable):
    """
    Pass through all values from the given iterable, augmented by the
    information if there are more values to come after the current one
    (True), or if it is the last value (False).
    Example:
        >>> for i, has_more in is_last_iteration(range(3)):
        ...     print(i, has_more)
    0 True
    1 True
    2 False
    """
    # Get an iterator and pull the first value.
    it = iter(iterable)
    last = next(it)
    # Run the iterator to exhaustion (starting from the second value).
    for val in it:
        # Report the *previous* value (more to come).
        yield last, False
        last = val
    # Report the last value.
    yield last, True


def text_int(text):
    return int(text) if text.isdigit() else text


def sort_keys(text):
    """
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    """
    return [text_int(c) for c in re.split(r"(\d+)", text)]


def get_files_from_dir(
    dir_path, valid_extensions=None, recursive=False, sort=False, to_img=False
):
    """
    @param dir_path:
    @type dir_path:
    @param valid_extensions:
    @type valid_extensions:
    @param recursive:
    @type recursive:
    @param sort:
    @type sort:
    @param to_img: does need the files to be converted as PIL.Image
    @type to_img:
    @return:
    @rtype: List[Path|Image]
    """
    path = check_path(dir_path)
    if recursive:
        files = [f.absolute() for f in path.glob("**/*") if f.is_file()]
    else:
        files = [f.absolute() for f in path.glob("*") if f.is_file()]

    if valid_extensions is not None:
        valid_extensions = (
            [valid_extensions]
            if isinstance(valid_extensions, str)
            else valid_extensions
        )
        valid_extensions = [
            f".{ext}" if not ext.startswith(".") else ext for ext in valid_extensions
        ]
        files = list(filter(lambda f: f.suffix in valid_extensions, files))

    for i in range(len(files)):
        files[i] = str(files[i])

    files = sorted(files, key=sort_keys) if sort else files

    for i in range(len(files)):
        files[i] = Path(files[i]) if not to_img else Image.open(files[i])

    return files


def clear_cuda():
    torch.cuda.empty_cache()
