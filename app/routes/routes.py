# import torch
# from IPython.display import Image
# from PIL import Image
# import os
# from flask import Flask, request, render_template, redirect, url_for, flash
#
# from werkzeug.utils import secure_filename
#
# from ..app import app
# from app.utils.const import OUTPUT_DIR, STATIC_DIR
#
# from app.TableExtractor.HTMLTranslator import HtmlTranslator
# from app.TableExtractor.utils.constant import (
#     ILLUSTRATION_LABEL,
#     TEXT_LABEL,
#     TABLE_WORD_LABEL,
#     LINE_LABEL,
#     COLUMN_LABEL,
# )
# from app.utils.utils import check_extension
#
#
# @app.route("/psw", methods=["GET", "POST"])
# def request_access():
#     # Here, a method to check if the user has the correct password to access the app
#     return render_template("pages/index.html")
#
#
# @app.route("/transcriber", methods=["GET", "POST"])
# def transcriber():
#     # Here, a method to retrieve the image(s) uploaded by the user and store it in /temp
#     # Then, detects the cells in the image(s), transcribe them
#     # Then returns an object that will be transform into an HTML table in order to be corrected by the user
#
#     # # # IMAGE UPLOAD
#     if request.method == "POST":
#         f = request.files["file"]  # request.files.get('image_name')
#         if f:
#             if check_extension(f.filename):
#                 filename = secure_filename(f.filename)
#                 f.save(os.path.join(OUTPUT_DIR, filename))
#                 filepath = os.path.join(STATIC_DIR, filename)
#                 # url_for('static', filename=filename)
#                 labels = [
#                     ILLUSTRATION_LABEL,
#                     TEXT_LABEL,
#                     TABLE_WORD_LABEL,
#                     LINE_LABEL,
#                     COLUMN_LABEL,
#                 ]
#
#                 img = Image.open(filepath)
#                 html_translator = HtmlTranslator(
#                     image=img,
#                     idx=0,
#                     labels_to_extract=labels,
#                     tag="four_branches_lines_columns_no_augmentation",
#                 )
#                 result = html_translator.execute()
#
#                 render_template(
#                     "pages/annotate.html",
#                     image=filepath,
#                     annotation=result,
#                     title="Upload image",
#                 )
#             else:
#                 flash(u"The file does not have the correct extension!", "error")
#         else:
#             flash(u"Error while importing the file!", "error")
#
#     return render_template("pages/import.html")
#
#
# @app.route("/", methods=["GET", "POST"])
# def transcription():
#     if request.method == "POST":
#         # image_name = request.files['file']
#         if "image_name" not in request.files:
#             return redirect(request.url)
#         image_name = request.files.get("image_name")
#         if not image_name:
#             return
#         im = Image.open(image_name)
#         html_translator = HtmlTranslator(
#             image=im,
#             idx=0,
#             labels_to_extract=[
#                 ILLUSTRATION_LABEL,
#                 TEXT_LABEL,
#                 TABLE_WORD_LABEL,
#                 LINE_LABEL,
#                 COLUMN_LABEL,
#             ],
#             tag="four_branches_lines_columns_no_augmentation",
#         )
#         html_translator.run()
#         os.system("rm -r 05*")
#         os.system("rm static/html-table_0.html")
#         os.system("rm static/annotated_0.jpg")
#         os.system("rm -r static/cell_images0")
#         os.system("mv html-table_0.html static")
#         os.system("mv annotated_0.jpg static")
#         os.system("mv cell_images0 static/cell_images0")
#
#         # return 'Transcription done!'
#         return render_template("pages/result.html")
#
#     return render_template("pages/index.html")
