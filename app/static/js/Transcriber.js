/* global $ */

class Transcriber {
    constructor(isTranscriber = true, output = null, div = "table-div") {
        this.tableDiv = typeof div === "string" ? $(`#${div}`) : div;
        this.scanImg = $("#scan");
        this.scanDiv = $("#scan-div");
        this.zoomedCell = $("#highlighted-cell");
        this.zoom = 2;

        this.output = output;
        this.currCell = new Cell();
        this.cells = [];
        this.maxRow = null;
        this.maxCol = null;
        this.htmlTable = "";
        this.scanWidth = null;
        this.scanHeight = null;
        this.baseX = null;
        this.baseY = null;

        /**
         * Is the object in transcriber mode, i.e. does it display input or only image (prepare page)?
         * @type {boolean}
         */
        this.isTranscriber = isTranscriber;

        // For dragging purposes
        this.isBeingDragged = false;
        this.mouseCoord = { x: null, y: null };
        this.scanOffset = null;
        this.scanDivWidth = null;
        this.scanDivHeight = null;

        this.protectedKeys = [13, 9, 46, 8, 27, 37, 38, 39, 40];
    }

    generateCheckBox(id, type = "row"){
        let checkbox = "";
        if (id === 1 && type === "col"){
            checkbox += `<th></th>`;
        }
        checkbox += `<th scope="${type}"><input id="check-${type}-${id}" type="checkbox" checked></th>`;
        return checkbox;
    }
    generateTable = (output = null) => {
        if (output){
            this.output = output;
        }
        this.maxRow = Math.max(...Object.keys(this.output).map(rowNb => Number(rowNb))) - 1;
        this.maxCol = Math.max(...Object.values(this.output).map(r => Object.keys(r).map(colNb => Number(colNb))).flat(2)) - 1;
        if (!this.isTranscriber){
            this.htmlTable += `<thead><tr>${
                [...Array(this.maxCol + 1).keys()]
                    .map(colIdx => this.generateCheckBox(colIdx + 1, "col"))
            }`;
        }
        let rowIdx = -1;
        for (let rowNb = 1; rowNb <= this.maxRow + 1; rowNb++) {
            if (this.output[String(rowNb)] === undefined){
                if (!this.isTranscriber){
                    this.output[String(rowNb)] = [];
                } else {
                    continue;
                }
            }

            this.htmlTable += `<tr id="row-${rowNb}">`;
            if (!this.isTranscriber){
                this.htmlTable += this.generateCheckBox(rowNb);
            }
            this.cells.push([]);
            rowIdx++;
            let colIdx = -1;
            for (let colNb = 1; colNb <= this.maxCol + 1; colNb++) {
                if (this.output[String(rowNb)][String(colNb)] === undefined){
                    if (!this.isTranscriber){
                        this.output[String(rowNb)][String(colNb)] = {img: null, coord: null, txt: "", dim: [null, null]};
                    } else {
                        continue;
                    }
                }

                colIdx++;
                const cell = this.output[String(rowNb)][String(colNb)] ?? {img: null, coord: null, txt: "", dim: [null, null]};
                this.addCell(new Cell(rowIdx, colIdx, cell.img, cell.coord, cell.txt, cell.dim));
            }
            this.htmlTable += "</tr>";
        }
        this.tableDiv.append(`<table><tbody>${this.htmlTable}</tbody></tbody>`);
        this.setEventListeners();

        // Reset table dimension because it might have been reduced due to the preparation phase
        this.maxRow = this.cells.length;
        this.maxCol = Math.max(...Object.values(this.output).map(r => Object.keys(r).length));
    }

    setEventListeners = () => {
        if (!this.isTranscriber) {
            return;
        }
        this.tableDiv.keyup(e => {
            e.preventDefault();
            switch(e.keyCode || e.which){
                case 13: // ENTER
                    this.currCell.validate();
                    this.focusCell(...this.next());
                    break;
                case 9: // TAB
                    break;
                case 46: // SUPPR
                    this.currCell.reset(true);
                    this.focusCell(...this.next());
                    break;
                case 8: // RETURN
                    this.currCell.getInput().val("");
                    break;
                case 27: // ESCAPE
                    this.focusOut();
                    break;
                case 38: // UP ARROW
                    this.focusCell(...this.above());
                    break;
                case 40: // DOWN ARROW
                    this.focusCell(...this.below());
                    break;
                case 37: // LEFT ARROW
                    this.focusCell(...this.prev());
                    break;
                case 39: // RIGHT ARROW
                    this.focusCell(...this.next());
                    break;
            }
        });
        const that = this;
        $("input").focus(function(){
            this.scrollIntoView({behavior: "smooth", block: "center", inline: "center"});
            that.changeCurrCell(... $(this).attr('id').split("-").map(n => Number(n)));
            that.currCell.zoomOnCoord();
        }).keydown((e) => {
            if (!this.protectedKeys.includes(e.keyCode || e.which)){
                if (this.currCell.status === "O"){
                    this.currCell.getInput().val("");
                }
                this.currCell.changeTxt();
            }
        })

        window.addEventListener('resize', function(){
            that.changeLayout();
        });
        window.addEventListener('click', function(e){
            that.isBeingDragged = false;
            if (!document.getElementById('transcriber').contains(e.target)){
                that.focusOut();
            } else if (document.getElementById('scan-div').contains(e.target)){
                that.isBeingDragged = true;
            }
        });

        this.scanDiv.on('mousemove', e => {
            if (this.isBeingDragged){
                // console.log("zigouigoui");
                //
                // const mousePosition = { x: e.pageX, y: e.pageY };
                // const offsetX = mousePosition.x - this.mouseCoord.x;
                // const offsetY = mousePosition.y - this.mouseCoord.y;
                // this.mouseCoord = { x: e.pageX, y: e.pageY }
                //
                // const coord = this.scanImg.css("transform").split(",").splice(4).map(e => Number(e.replace(/\D/g,'')));
                //
                // const newX = (coord[0] / this.zoom) + offsetX + this.baseX;
                // const newY = (coord[1] / this.zoom) + offsetY + this.baseY;
                //
                // this.zoomOnCoord(newX, newY);
            }
        });
        this.changeLayout();
    }

    toTSV = () => {
        // here transform input value to TSV
        let tsv = "";
        this.cells.map(row => {
            row.map(cell => {
                tsv += `${cell.getTxt()}\t`;
            });
            // remove last tab at the end of the line
            tsv = `${tsv.slice(0, -1)}\n`;
        })
        download(tsv, `table${this.id ?? 0}`, "tsv");
    }

    getLinesToRemove = () => {
        let rowCol = {
            col: [],
            row: []
        }
        Object.values($("input")).map(input => {
            if (input.checked === false){
                rowCol[input.id.includes("row") ? "row" : "col"].push(input.id.replace(/\D/g, ""));
            }
        });
        return rowCol;
    }

    getJsonToTranscribe = () => {
        const rowCol = this.getLinesToRemove();
        let table = JSON.parse(JSON.stringify(this.output));
        let imgs = [];
        Object.keys(this.output).map(row => {
            Object.keys(this.output[row]).map(col => {
                if (rowCol.col.includes(col)){
                    delete table[row][col]
                } else if (!rowCol.row.includes(row)) {
                    imgs.push(`${row},${col}`)
                }
            })
            if (rowCol.row.includes(row)){
                delete table[row]
            }
        })
        return JSON.stringify({
            table: table,
            imgs: imgs
        });
    }

    /**
     * Add a cell to the Transcriber as well to the HTML table
     * @param {Cell} cell
     */
    addCell = (cell) => {
        if (this.cells[cell.rowNb] === undefined){
            console.log(this.cells, cell.rowNb);
            this.cells[cell.rowNb] = [];
        }
        this.cells[cell.rowNb][cell.colNb] = cell;
        this.htmlTable += cell.generateTd(!this.isTranscriber);
        cell.setParent(this);
    }

    changeLayout = () => {
        if (!this.isTranscriber){
            return;
        }
        document.getElementById("scan-div").clientWidth
        this.zoom = 2; // todo change zoom to result to a zoom cell approximately 1/6 of the width of the scan

        this.baseX = (this.scanWidth / 2) * this.zoom;
        this.baseY = (this.scanHeight / 2) * this.zoom;

        // this.scanOffset = this.scanDiv.offset();
        // this.scanDivWidth = this.scanDiv.outerWidth();
        // this.scanDivHeight = this.scanDiv.outerHeight();

        if (this.currCell.parent){
            this.currCell.zoomOnCoord();
        }
    }

    setScanDim = (w, h) => {
        this.scanWidth = w;
        this.scanHeight = h;
    }

    changeCurrCell = (row, col) => {
        let cell = this.cells[row][col];
        if (cell !== undefined){
            this.currCell = cell;
        }
    }

    focusCell = (row, col) => {
        if (this.cells[row] === undefined){
            console.log("Unknown row")
            console.table({
                row: `current: ${this.currCell.rowNb}, desired: ${row}, max: ${this.maxRow}`,
                col: `current: ${this.currCell.colNb}, desired: ${col}, max: ${this.maxCol}`
            });
            console.log(this.cells);
            return;
        } else if (this.cells[row][col] === undefined){
            console.log("Unknown column")
            console.table({
                row: `current: ${this.currCell.rowNb}, desired: ${row}, max: ${this.maxRow}`,
                col: `current: ${this.currCell.colNb}, desired: ${col}, max: ${this.maxCol}`
            });
            console.log(this.cells[row]);
            return;
        }
        this.cells[row][col].focus();
    }

    focusOut = () => {
        this.zoomedCell.hide();
        this.scanImg.css("transform", "");
        this.scanImg.removeClass("zoomed");
        this.currCell.getInput().focusout();
        this.currCell = new Cell();
    }

    next = () => {
        let col = Number(this.currCell.colNb) ?? 0;
        let row = Number(this.currCell.rowNb) ?? 0;
        if (Number(this.currCell.colNb) < this.maxCol - 1){
            col = col + 1;
        } else if (Number(this.currCell.rowNb) < this.maxRow - 3){
            row = row + 1;
            col = 0;
        } else {
            console.log("end of table");
            row = 0;
            col = 0;
        }
        return [row, col]
    }
    prev = () => {
        let col = Number(this.currCell.colNb) ?? 0;
        let row = Number(this.currCell.rowNb) ?? 0;
        if (Number(this.currCell.colNb) > 0){
            col = col - 1;
        } else if (Number(this.currCell.rowNb) > 0){
            row = row - 1;
            col = this.maxCol - 1;
        } else {
            console.log("start of table");
            row = this.maxRow - 1;
            col = this.maxCol - 1;
        }
        return [row, col]
    }
    above = () => {
        const isFirst = Number(this.currCell.rowNb) === 0;
        return [
            isFirst ? this.maxRow - 1 : Number(this.currCell.rowNb) - 1,
            Number(this.currCell.colNb)
        ];
    };
    below = () => {
        const isLast = Number(this.currCell.rowNb) === this.maxRow - 1;
        return [
            isLast ? 0 : Number(this.currCell.rowNb) + 1,
            Number(this.currCell.colNb)
        ];
    }

    zoomOnCoord = (x, y) => {
        if (x === null && y === null){
            return
        }
        const offsetX = this.baseX - (x * this.zoom);
        const offsetY = this.baseY - (y * this.zoom);
        this.scanImg
            .addClass("zoomed")
            .css("transform", `translate(${offsetX}px, ${offsetY}px) scale(${this.zoom})`);

        // this.zoomedCell.hide();
    }

    showZoomedCell = (w, h) => {
        this.zoomedCell.show()
            .css("width", `${(w * this.zoom) || 50}px`)
            .css("height", `${(h * this.zoom) || 50}px`);
    }
}
