class Cell {
    /**
     * @param {Integer|null} rowNb
     * @param {Integer|null} colNb
     * @param img
     * @param coord
     * @param txt
     * @param dim
     */
    constructor(rowNb = null, colNb = null, img = null, coord = "0,0", txt = "", dim = [0,0]) {
        this.img = img;
        this.input = null;

        this.txt = txt;
        this.origTxt = txt;
        this.status = "O"; // O = original, V = validated, C = corrected

        this.colNb = colNb;
        this.rowNb = rowNb;

        this.imgWidth = dim[0];
        this.imgHeight = dim[1];

        coord = this.formatCoord(coord);
        this.x = coord[0];
        this.y = coord[1];
    }

    /**
     *
     * @param {Transcriber} parent
     */
    setParent(parent){
        this.parent = parent;
    }

    formatCoord = coord => {
        switch(getType(coord)){
            case "undefined":
                return [null, null];
            case "string":
                return coord.split(",").map(nb => Number(nb));
            case "array":
                return coord.map(nb => Number(nb));
            case "object":
                return Object.values(coord).map(nb => Number(nb));
            default:
                return [null, null];
        }
    }

    generateTd = (onlyImg = false) => {
        const img = this.img ? `<img id="img-${this.rowNb}-${this.colNb}" class="cell-img" src="${outputs}/(${this.img}).jpg" alt="Cell">` : "";
        const inp = onlyImg ? "" : `<input id="${this.rowNb}-${this.colNb}" value="${this.txt ?? ""}" type="text">`
        return `<td class="cell row-${this.rowNb} col-${this.colNb}">
                    <div style="height: 30px">${img}</div>
                    ${inp}
                </td>`;
    }
    setInput = () => {
        this.input = $(`#${this.rowNb}-${this.colNb}`);
    }
    getInput = () => {
        if (!this.input){
            this.setInput();
        }
        return this.input;
    }
    focus = () => {
        this.getInput().focus();
    }

    getTxt = () => {
        this.txt = this.getInput().val();
        return this.txt
    }

    changeTxt = () => {
        if (this.wasModified()){
            this.correct()
        } else {
            this.reset()
        }
    }

    wasModified = () => this.getTxt() !== this.origTxt

    reset = (resetVal = false) => {
        this.getInput().removeClass("validated corrected");
        if (resetVal){
            this.input.val(this.origTxt);
        }
        this.status = "O"
    }
    correct = () => {
        this.reset();
        this.input.addClass("corrected");
        this.status = "C"
    }
    confirm = () => {
        this.reset();
        this.input.addClass("validated");
        this.status = "V"
    }
    validate = () => {
        if (this.wasModified()){
            this.correct();
        } else {
            this.confirm()
        }
    }
    zoomOnCoord = () => {
        this.parent.zoomOnCoord(this.x, this.y);
        this.parent.showZoomedCell(this.imgWidth, this.imgHeight)
    }
}
