function preview_image(event) {
    let div = document.createElement("div");
    div.appendChild(document.createTextNode("Ready to run!"));
    document.body.appendChild(div);
    let reader = new FileReader();
    reader.onload = function(){
        let output = document.getElementById('output-image')
        output.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
}

const isArray = function (a) {
    return Array.isArray(a);
};

const isObject = function (o) {
    return o === Object(o) && !isArray(o) && typeof o !== "function";
};

getType = variable => {
    switch(typeof variable){
        case "object":
            if (variable === null){
                return "undefined"
            }
            if (isArray(variable)){
                return "array";
            }
            return isObject(variable) ? "object" : "weirdo"
        default:
            return typeof variable;
    }
}

const download = (content, filename, extension) => {
    if (!["csv", "tsv", "txt", "json"].includes(extension)){
        return;
    }
    const blob = new Blob([content],{type:`text/${extension};charset=utf-8;`});
    const link = document.createElement('a');
    link.style.visibility='hidden';

    if (link.download === undefined){
        document.body.removeChild(link);
        return;
    }
    link.setAttribute('href', window.URL.createObjectURL(blob));
    link.setAttribute('download',`${filename}.${extension}`);

    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);

    // JSON
    /*let a = document.createElement("a");
    let header = "";
    if (format === "json") {
        header = `data:text/json;charset=utf-8,`;
    }
    a.setAttribute("href", `${header}${content}`);
    a.setAttribute("download", `${fileName}.${format}`);
    a.click();
    a.remove();*/
}
