# TableTranscriber

## Set-up

### 0.Prerequisites

- python3
- pip3
- conda

### 1.Download

- download the repo

### 2.Install requirements

- Create a virtual environment for the project:

```bash
python3.10 -m venv venv  # venv is the name of the virtual env
source venv/bin/activate
pip install -r requirements.txt        
```

- Enable pre-commit hooks
```bash
pre-commit install
```

> These hooks will auto-format and test your code whenever you try to commit changes.
>
> Hooks enabled :
> - end-of-file-fixer (Makes sure files end in a newline and only a newline)
> - trailing-whitespace (Trims trailing whitespace)
> - black (Auto-formats code)

### 3.Run app

- (connect yourself to GPU via SSH)

- Make sure your virtual environment is activated, if not run `conda activate tableTranscriber`

- Launch Flask application:
```bash
python3 run.py
```

- Visit [`localhost:5000`](localhost:5000)
